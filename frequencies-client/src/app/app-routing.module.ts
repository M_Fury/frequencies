import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainMenuComponent } from './main-menu/main-menu.component';
import { FrequencyGeneratorComponent } from './frequency-generator/frequency-generator.component';
import { FrequencyDownloaderComponent } from './frequency-downloader/frequency-downloader.component';
import { HttpErrorComponent } from './http-error/http-error.component';
import { ViewAllFrequenciesComponent } from './view-all-frequencies/view-all-frequencies.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainMenuComponent },
  { path: 'generate-new-frequency', component: FrequencyGeneratorComponent },
  { path: 'download-saved-frequency', component: FrequencyDownloaderComponent },
  { path: 'http-error', component: HttpErrorComponent },
  { path: 'view-frequencies', component: ViewAllFrequenciesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
