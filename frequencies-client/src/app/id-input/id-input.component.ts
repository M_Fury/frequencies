import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FrequencyManagerService } from '../frequency-manager.service';
import { Message } from '../message';

@Component({
  selector: 'app-id-input',
  templateUrl: './id-input.component.html',
  styleUrls: ['./id-input.component.css'],
})
export class IdInputComponent implements OnInit {
  id: string = '';

  message: Message = { text: '', error: false };

  @Input() idMessages: { [key: string]: Message } = {};

  @Output() idChanged = new EventEmitter<string>();
  @Output() onHttpError = new EventEmitter<HttpErrorResponse>();

  constructor(private frequencyManager: FrequencyManagerService) {}

  ngOnInit(): void {}

  onKey() {
    this.idChanged.emit(this.id);
    this.checkId();
  }

  async checkId() {
    if (this.id == '') {
      this.message = this.idMessages.EMPTY;
      return;
    }

    try {
      const EXISTS: boolean = await this.frequencyManager.isExists(this.id);

      if (EXISTS) {
        this.message = this.idMessages.EXISTS;
      } else {
        this.message = this.idMessages.DOES_NOT_EXIST;
      }
    } catch (error) {
      this.onHttpError.emit(error);
    }
  }
}
