import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FrequencyManagerService } from '../frequency-manager.service';
import { Message } from '../message';
import { ID_MESSAGES } from './id-messages';
import { GENERATE_MESSAGES } from './generation-messages';

@Component({
  selector: 'app-frequency-generator',
  templateUrl: './frequency-generator.component.html',
  styleUrls: ['./frequency-generator.component.css'],
})
export class FrequencyGeneratorComponent implements OnInit {
  idMessages = ID_MESSAGES;

  generationMessage: Message = { text: '', error: false };

  id: String = '';
  size: number = 32;
  generatedId: String = '';
  generatedSize: number = 32;

  showError: boolean = false;
  httpError: HttpErrorResponse;

  constructor(private frequencyManager: FrequencyManagerService) {}

  ngOnInit(): void {}

  updateId(newId: string) {
    this.id = newId;
  }

  async generateNewFrequency() {
    this.generationMessage = { text: '', error: false };

    this.generatedId = this.id;
    this.generatedSize = this.size;

    try {
      await this.frequencyManager.generateNewFrequency(
        this.generatedId,
        this.generatedSize
      );
      this.generationMessage = GENERATE_MESSAGES.SUCCESS;
    } catch (error) {
      if (error.isArgumentsError) {
        this.generationMessage = GENERATE_MESSAGES.FAILURE;
      } else {
        this.showHttpError(error.httpError);
      }
    }
  }

  showHttpError(error: HttpErrorResponse) {
    this.httpError = error;
    this.showError = true;
  }

  removeHttpError() {
    this.showError = false;
    this.id = '';
  }
}
