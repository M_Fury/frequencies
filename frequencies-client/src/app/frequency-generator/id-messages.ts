import { Message } from '../message';

export const ID_MESSAGES: { [key: string]: Message; } = {
    "EMPTY": {
        text: "✘ The id field must not be empty.",
        error: true
    },
    "EXISTS": {
        text: "✘ This id already exists.",
        error: true
    },
    "DOES_NOT_EXIST": {
        text: "✔",
        error: false
    }
};