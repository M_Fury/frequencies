import { Message } from '../message';

export const GENERATE_MESSAGES: { [key: string]: Message; } = {
    "SUCCESS": {
        text: "was generated and saved successfully.\n\
          You can now download the frequency:",
        error: false
    },
    "FAILURE": {
        text: "failed to be generated and saved.\n\
          Please try again with a different ID.",
        error: true
    }
};