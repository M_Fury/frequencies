import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FrequencyDownloaderComponent } from './frequency-downloader/frequency-downloader.component';
import { FrequencyGeneratorComponent } from './frequency-generator/frequency-generator.component';
import { DownloadButtonComponent } from './download-button/download-button.component';
import { IdInputComponent } from './id-input/id-input.component';
import { AppRoutingModule } from './app-routing.module';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { HttpErrorComponent } from './http-error/http-error.component';
import { ViewAllFrequenciesComponent } from './view-all-frequencies/view-all-frequencies.component';

@NgModule({
  declarations: [
    AppComponent,
    FrequencyDownloaderComponent,
    FrequencyGeneratorComponent,
    DownloadButtonComponent,
    IdInputComponent,
    MainMenuComponent,
    HttpErrorComponent,
    ViewAllFrequenciesComponent,
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
