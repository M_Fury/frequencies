import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FrequencyManagerService } from '../frequency-manager.service';
import { saveAs } from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-view-all-frequencies',
  templateUrl: './view-all-frequencies.component.html',
  styleUrls: ['./view-all-frequencies.component.css'],
})
export class ViewAllFrequenciesComponent implements OnInit {
  frequenciesIds: string[];

  showError: boolean = false;
  httpError: HttpErrorResponse;

  constructor(private frequencyManager: FrequencyManagerService) {}

  async ngOnInit() {
    try {
      this.frequenciesIds = await this.frequencyManager.getAllFrequencies();
    } catch (error) {
      this.showHttpError(error.httpError);
    }
  }

  async download(id: string) {
    try {
      const BLOB: Blob = await this.frequencyManager.downloadSavedFrequency(id);

      saveAs(BLOB, 'frequency_' + id + '.bin');
    } catch (error) {
      this.showHttpError(error.httpError);
    }
  }

  showHttpError(error: HttpErrorResponse) {
    this.httpError = error;
    this.showError = true;
  }

  removeHttpError() {
    this.showError = false;
  }
}
