import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { FrequencyManagerService } from '../frequency-manager.service';
import { Message } from '../message';

import { DOWNLOAD_MESSAGES } from './download-messages';

@Component({
  selector: 'app-download-button',
  templateUrl: './download-button.component.html',
  styleUrls: ['./download-button.component.css'],
})
export class DownloadButtonComponent implements OnInit {
  message: Message = { text: '', error: false };

  @Input() id: String = '';

  @Output() onHttpError = new EventEmitter<HttpErrorResponse>();

  constructor(private frequencyManager: FrequencyManagerService) {}

  ngOnInit(): void {}

  async download() {
    if (this.id == '') {
      this.message = DOWNLOAD_MESSAGES.NO_ID;
      return;
    }

    this.message = { text: '', error: false };

    try {
      const BLOB: Blob = await this.frequencyManager.downloadSavedFrequency(
        this.id
      );

      saveAs(BLOB, 'frequency_' + this.id + '.bin');
      this.message = DOWNLOAD_MESSAGES.SUCCESS;
    } catch (error) {
      if (error.isIdError) {
        this.message = DOWNLOAD_MESSAGES.FAILURE;
      } else {
        this.onHttpError.emit(error.httpError);
      }
    }
  }
}
