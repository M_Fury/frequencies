import { Message } from '../message';

export const DOWNLOAD_MESSAGES: { [key: string]: Message } = {
  SUCCESS: {
    text: 'Downloaded the frequency seccessfully.',
    error: false,
  },
  FAILURE: {
    text: 'Failed to download the frequency.',
    error: true,
  },
  NO_ID: {
    text: "Can't download a frequency without an id.",
    error: true,
  },
};
