export interface Message {
    text: string;
    error: boolean;
}
