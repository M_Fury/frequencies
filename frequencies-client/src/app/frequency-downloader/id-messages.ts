import { Message } from '../message';

export const ID_MESSAGES: { [key: string]: Message } = {
  EMPTY: {
    text: '✘ The id field must not be empty.',
    error: true,
  },
  EXISTS: {
    text: '✔',
    error: false,
  },
  DOES_NOT_EXIST: {
    text: '✘ This id does not exist.',
    error: true,
  },
};
