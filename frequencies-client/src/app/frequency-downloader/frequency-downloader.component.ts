import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ID_MESSAGES } from './id-messages';

@Component({
  selector: 'app-frequency-downloader',
  templateUrl: './frequency-downloader.component.html',
  styleUrls: ['./frequency-downloader.component.css'],
})
export class FrequencyDownloaderComponent implements OnInit {
  idMessages = ID_MESSAGES;

  id: String = '';

  showError: boolean = false;
  httpError: HttpErrorResponse;

  constructor() {}

  ngOnInit(): void {}

  updateId(newId: string) {
    this.id = newId;
  }

  showHttpError(error: HttpErrorResponse) {
    this.httpError = error;
    this.showError = true;
  }

  removeHttpError() {
    this.showError = false;
    this.id = '';
  }
}
