import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class FrequencyManagerService {
  readonly serverUrl = environment.serverUrl;

  constructor(private http: HttpClient) {}

  generateNewFrequency(id: String, size: Number): Promise<{}> {
    const URL: string = `${this.serverUrl}/frequencies/save?id=${id}&size=${size}`;

    return new Promise((resolve, reject) => {
      this.http.post(URL, '', { responseType: 'text' }).subscribe(
        () => resolve(),
        (error) => {
          if (error.status != 400) {
            reject({ isArgumentsError: false, httpError: error });
          }
          reject({ isArgumentsError: true, httpError: error });
        }
      );
    });
  }

  downloadSavedFrequency(id: String): Promise<Blob> {
    const URL: string = `${this.serverUrl}/frequencies/${id}/`;

    return new Promise((resolve, reject) => {
      this.http.get(URL, { responseType: 'blob' }).subscribe(
        (response) => resolve(response),
        (error) => {
          if (error.status != 404) {
            reject({ isIdError: false, httpError: error });
          }
          reject({ isIdError: true, httpError: error });
        }
      );
    });
  }

  isExists(id: String): Promise<boolean> {
    const URL: string = `${this.serverUrl}/frequencies/isExists?id=${id}`;

    return new Promise((resolve, reject) => {
      this.http.get<boolean>(URL).subscribe(
        (response) => resolve(response),
        (error) => {
          reject(error);
        }
      );
    });
  }

  getAllFrequencies(): Promise<string[]> {
    const URL: string = `${this.serverUrl}/frequencies/getAll`;

    return new Promise((resolve, reject) => {
      this.http.get(URL).subscribe(
        (response) => resolve(JSON.parse(JSON.stringify(response)).sort()),
        (error) => reject(error)
      );
    });
  }
}
