import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-http-error',
  templateUrl: './http-error.component.html',
  styleUrls: ['./http-error.component.css'],
})
export class HttpErrorComponent implements OnInit {
  @Input() error: HttpErrorResponse;
  @Output() pressedBack = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  goBack() {
    this.pressedBack.emit();
  }
}
