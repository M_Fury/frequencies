package com.training.frequenciesserver;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

@ActiveProfiles(profiles = "dev")
@SpringBootTest
public class FrequencyRepositoryTests {
    private static final String REGULAR_ID = "Bari";
    private static final int FREQUENCY_MAX_SIZE = 64;
    private static final byte[] MAX_SIZE_FREQUENCY = new byte[FREQUENCY_MAX_SIZE];

    private static Frequency REGULAR_FREQUENCY;

    @Autowired
    private FrequencyRepository frequenciesRepository;

    @BeforeAll
    static void setUp(){
        new Random().nextBytes(MAX_SIZE_FREQUENCY);

        REGULAR_FREQUENCY = new Frequency(REGULAR_ID, MAX_SIZE_FREQUENCY);
    }

    @BeforeEach
    void init(){
        frequenciesRepository.deleteAll();

        frequenciesRepository.save(REGULAR_FREQUENCY);
    }

    @AfterEach
    void clear() {
        frequenciesRepository.deleteAll();
    }

    @Test
    public void givenDbWithFilledFrequencyTable_whenFindById_thanReturnNotNull() {
        assertNotNull(frequenciesRepository);
    }

    @Test
    public void givenStoredFrequency_whenFindById_thanReturnFrequency() {
        Optional<Frequency> frequency = frequenciesRepository.findById(REGULAR_ID);

        assertTrue(frequency.isPresent(), "didn't return frequency while should have");
    }

    @Test
    public void givenEmptyDb_whenFindById_thanReturnEmptyOptional() {
        frequenciesRepository.deleteAll();

        Optional<Frequency> frequency = frequenciesRepository.findById(REGULAR_ID);

        assertFalse(frequency.isPresent(), "returned frequency while db is empty");
    }

    @Test
    public void givenStoredFrequency_whenFindById_thanReturnFrequencyWithExpectedFrequency() {
        Optional<Frequency> frequency = frequenciesRepository.findById(REGULAR_ID);

        assertTrue(frequency.isPresent(), "returned null while should returned frequency");
        byte[] frequency_data = frequency.get().getFrequency();

        boolean isEquals = Arrays.equals(frequency_data, MAX_SIZE_FREQUENCY);

        assertTrue(isEquals, "got different frequencies when expected equals");
    }

    @Test
    public void givenStoredFrequency_whenFindAllIds_thanReturnArrayListWithExpectedId() {
        String[] actualIds = frequenciesRepository.findAllIds();
        String[] expectedIds = { REGULAR_ID };

        boolean isEquals = Arrays.equals(actualIds, expectedIds);

        assertTrue(isEquals,"findAllIds" +
                "\nexpected " + Arrays.toString(expectedIds) +
                "\nactual " + Arrays.toString(actualIds));
    }
}