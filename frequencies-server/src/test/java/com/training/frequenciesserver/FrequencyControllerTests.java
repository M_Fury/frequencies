package com.training.frequenciesserver;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(controllers = FrequencyController.class)
class FrequencyControllerTests {
    private static final int FIXED_FREQUENCY_SIZE = 32;
    private static final int SECOND_FREQUENCY_SIZE = 64;
    private static final int FREQUENCY_INVALID_SIZE = 666;

    private static final String REGULAR_ID = "furyGhost";
    private static final String SECOND_REGULAR_ID = "fury";

    private static final String GET_FREQUENCY_URL = "/frequencies/{id}";
    private static final String GET_ALL_URL = "/frequencies/getAll";
    private static final String POST_FREQUENCY_URL = "/frequencies/save?id={id}&size={size}";
    private static final String IS_EXISTS_URL = "/frequencies/isExists?id={id}";

    private static final String ACCESS_HEADER_NAME = "Access-Control-Allow-Origin";

    @Value("${client-url}")
    private String clientUrl;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    FrequencyService mockFrequencyService;

    @BeforeEach
    void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    /******************************************saveFrequencyById-Tests*************************************************/
    @Test
    void whenSaveToFrequencyEndpoint_shouldHaveHttpOkStatusAndExpectedHeader() throws Exception {
        System.out.println(clientUrl);

        when(mockFrequencyService.generateAndSaveFrequency(REGULAR_ID, FIXED_FREQUENCY_SIZE)).thenReturn(true);

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(post(POST_FREQUENCY_URL, REGULAR_ID, FIXED_FREQUENCY_SIZE))
                .andReturn()
                .getResponse();

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResponse.getStatus()),
                () -> assertEquals(clientUrl, mvcResponse.getHeader(ACCESS_HEADER_NAME))
        );
    }

    @Test
    void whenGenerateAndSaveUnfixedFrequencySize_shouldReturnResponseWithHttpBadRequestStatus() throws Exception {
        when(mockFrequencyService.generateAndSaveFrequency(REGULAR_ID, FREQUENCY_INVALID_SIZE)).thenReturn(false);

        this.mockMvc
                .perform(post(POST_FREQUENCY_URL, REGULAR_ID, FREQUENCY_INVALID_SIZE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void givenSavedByNameFrequency_whenSaveFrequencyBySameName_shouldGetHttpBadRequestStatus() throws Exception {
        when(mockFrequencyService.generateAndSaveFrequency(REGULAR_ID, FIXED_FREQUENCY_SIZE)).thenReturn(false);

        this.mockMvc
                .perform(post(POST_FREQUENCY_URL, REGULAR_ID, FIXED_FREQUENCY_SIZE))
                .andExpect(status().isBadRequest());
    }

    /******************************************getFrequencyById-Tests**************************************************/
    @Test
    void whenGetToFrequencyEndpoint_shouldReturnResponseWithHttpOkStatusAndExpectedHeader() throws Exception {
        when(mockFrequencyService.getFrequencyById(REGULAR_ID)).thenReturn(Optional.of(new byte[FIXED_FREQUENCY_SIZE]));

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(get(GET_FREQUENCY_URL, REGULAR_ID))
                .andReturn()
                .getResponse();

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResponse.getStatus()),
                () -> assertEquals(clientUrl, mvcResponse.getHeader(ACCESS_HEADER_NAME))
        );
    }

    @Test
    void whenGetToFrequencyEndpoint_shouldReturnResponseWithContentTypeOctecStream() throws Exception {
        when(mockFrequencyService.getFrequencyById(REGULAR_ID)).thenReturn(Optional.of(new byte[FIXED_FREQUENCY_SIZE]));

        this.mockMvc
                .perform(get(GET_FREQUENCY_URL, REGULAR_ID))
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM));
    }

    @Test
    void givenMatchingSavedNameAndFrequency_whenGetFrequencyByName_shouldReturnFrequencyExpectedLength() throws Exception {
        when(mockFrequencyService.getFrequencyById(REGULAR_ID)).thenReturn(Optional.of(new byte[FIXED_FREQUENCY_SIZE]));

        this.mockMvc
                .perform(get(GET_FREQUENCY_URL, REGULAR_ID))
                .andExpect(content().bytes(new byte[FIXED_FREQUENCY_SIZE]));
    }

    @Test
    void whenGettingTwoFrequencies_shouldReturnDifferentData() throws Exception {
        when(mockFrequencyService.getFrequencyById(REGULAR_ID)).thenReturn(Optional.of(new byte[FIXED_FREQUENCY_SIZE]));
        when(mockFrequencyService.getFrequencyById(SECOND_REGULAR_ID)).thenReturn(Optional.of(new byte[SECOND_FREQUENCY_SIZE]));

        byte[] firstFrequency = getFrequencyFromMockMvc(REGULAR_ID);
        byte[] secondFrequency = getFrequencyFromMockMvc(SECOND_REGULAR_ID);

        boolean isFrequenciesEqual = Arrays.equals(firstFrequency, secondFrequency);

        assertFalse(isFrequenciesEqual, "Got 2 equal frequencies: " + Arrays.toString(firstFrequency));
    }

    @Test
    void givenNonExistingId_whenGetFrequencyById_shouldReturnResponseWithHttpNotFoundStatus() throws Exception {
        when(mockFrequencyService.getFrequencyById(REGULAR_ID)).thenReturn(Optional.empty());

        this.mockMvc
                .perform(get(GET_FREQUENCY_URL, REGULAR_ID))
                .andExpect(status().isNotFound());
    }

    /******************************************getAll-Tests************************************************************/
    @Test
    void whenGetAll_shouldReturnResponseWithHttpOkStatusAndExpectedHeaders() throws Exception {
        when(mockFrequencyService.getAllIds()).thenReturn(new String[]{ REGULAR_ID });

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(get(GET_ALL_URL))
                .andReturn()
                .getResponse();

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResponse.getStatus()),
                () -> assertEquals(clientUrl, mvcResponse.getHeader(ACCESS_HEADER_NAME)),
                () -> assertEquals(MediaType.APPLICATION_JSON_VALUE, mvcResponse.getContentType())
        );
    }

    @Test
    void whenGetAll_shouldReturnResponseWithJsonWithExpectedId() throws Exception {
        when(mockFrequencyService.getAllIds()).thenReturn(new String[]{ REGULAR_ID });

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(get(GET_ALL_URL))
                .andReturn()
                .getResponse();

        String expectedBody = "[\"" + REGULAR_ID + "\"]";

        assertEquals(expectedBody, mvcResponse.getContentAsString());
    }

    /**********************************************isExists-Tests******************************************************/
    @Test
    void givenSavedName_whenChecksIfExists_shouldReturnResponseWithHttpOkStatusAndExpectedHeaderAndTrue() throws Exception {
        when(mockFrequencyService.isExists(REGULAR_ID)).thenReturn(true);

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(get(IS_EXISTS_URL, REGULAR_ID))
                .andReturn()
                .getResponse();

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResponse.getStatus()),
                () -> assertEquals(clientUrl, mvcResponse.getHeader(ACCESS_HEADER_NAME)),
                () -> assertEquals("true", mvcResponse.getContentAsString())
                );
    }

    @Test
    void givenUnSavedName_whenChecksIfExists_shouldReturnResponseWithHttpOkStatusAndExpectedHeaderAndFalse() throws Exception {
        when(mockFrequencyService.isExists(REGULAR_ID)).thenReturn(false);

        MockHttpServletResponse mvcResponse = this.mockMvc
                .perform(get(IS_EXISTS_URL, REGULAR_ID))
                .andReturn()
                .getResponse();

        assertAll(
                () -> assertEquals(HttpStatus.OK.value(), mvcResponse.getStatus()),
                () -> assertEquals(clientUrl, mvcResponse.getHeader(ACCESS_HEADER_NAME)),
                () -> assertEquals("false", mvcResponse.getContentAsString())
        );
    }

    private byte[] getFrequencyFromMockMvc(String id) throws Exception {
        return this.mockMvc
                .perform(get(GET_FREQUENCY_URL, id))
                .andReturn()
                .getResponse()
                .getContentAsByteArray();
    }
}