package com.training.frequenciesserver;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FrequencyServiceTests {
    private static final int FIXED_FREQUENCY_SIZE = 32;
    private static final int FREQUENCY_INVALID_SIZE = 666;

    private static final String REGULAR_ID = "furyGhost";
    private static final String EMPTY_ID = "";

    @InjectMocks
    FrequencyService injectedFrequencyService = new FrequencyService();

    @Mock
    private FrequencyRepository mockedFrequenciesRepository;

    static Frequency frequency;

    @BeforeAll
    public static void setUp(){
        byte[] frequency = new byte[FIXED_FREQUENCY_SIZE];
        new Random().nextBytes(frequency);

        FrequencyServiceTests.frequency = new Frequency(REGULAR_ID, frequency);
    }

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**********************************************generateAnsSaveFrequency-Tests**************************************/
    @Test
    void whenGenerateAndSaveFrequencyWithFixedSize_shouldReturnTrue() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.empty());

        boolean isSaved = injectedFrequencyService.generateAndSaveFrequency(REGULAR_ID, FIXED_FREQUENCY_SIZE);

        assertTrue(isSaved, "didn't managed to generate and save frequency");
    }

    @Test
    void whenGenerateAndSaveFrequencyWithInvalidSize_shouldReturnFalse() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.empty());

        boolean isSaved = injectedFrequencyService.generateAndSaveFrequency(REGULAR_ID, FREQUENCY_INVALID_SIZE);

        assertFalse(isSaved, "managed to generate and save frequency while shouldn't because of invalid size input");
    }

    @Test
    void givenSavedFrequencyWithId_whenSaveAndGenerateWithSameId_shouldGetFalse() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.of(frequency));

        boolean isError = injectedFrequencyService.generateAndSaveFrequency(REGULAR_ID, FIXED_FREQUENCY_SIZE);

        assertFalse(isError, "should got error for trying to save frequency with an already saved id but didn't");
    }

    @Test
    void whenSaveAndGenerateWithEmptyId_shouldGetFalse() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.empty());

        boolean isError = injectedFrequencyService.generateAndSaveFrequency(EMPTY_ID, FIXED_FREQUENCY_SIZE);

        assertFalse(isError, "should got error for trying to save frequency empty id");
    }

    /**********************************************getFrequencyById-Tests**********************************************/
    @Test
    void givenSavedFrequency_whenGetFrequencyById_thanReturnFrequencyWithExpectedLength() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.of(frequency));

        Optional<byte[]> frequency = injectedFrequencyService.getFrequencyById(REGULAR_ID);

        assertTrue(frequency.isPresent(), "didn't returned frequency");
        assertEquals(FIXED_FREQUENCY_SIZE, frequency.get().length);
    }

    @Test
    void givenEmptyDb_whenGetFrequencyById_thanReturnEmptyOptional() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.empty());

        Optional<byte[]> frequency = injectedFrequencyService.getFrequencyById(REGULAR_ID);

        assertFalse(frequency.isPresent(), "returned frequency while should return empty Optional");
    }

    /*********************************************getAll-Tests*********************************************************/
    @Test
    void givenSavedFrequency_whenGetAll_thanReturnArrayWithId() {
        when(mockedFrequenciesRepository.findAllIds()).thenReturn(new String[]{ REGULAR_ID });

        String[] ids = injectedFrequencyService.getAllIds();

        boolean isEqual = Arrays.equals(ids, new String[]{ REGULAR_ID });

        assertTrue(isEqual, "didn't return array with the expected id");
    }

    /*********************************************isExists-Tests*******************************************************/
    @Test
    void givenNoFrequency_whenIsExists_shouldGetFalse() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.empty());

        assertFalse(injectedFrequencyService.isExists(REGULAR_ID), "check if a non existing id exists and got true");
    }

    @Test
    void givenSavedFrequency_whenIsExists_shouldGetTrue() {
        when(mockedFrequenciesRepository.findById(REGULAR_ID)).thenReturn(Optional.of(frequency));

        assertTrue(injectedFrequencyService.isExists(REGULAR_ID), "check if an existing id exists and got false");
    }
}