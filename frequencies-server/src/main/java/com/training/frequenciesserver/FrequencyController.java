package com.training.frequenciesserver;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/frequencies")
public class FrequencyController {
    private static final int EMPTY_ARRAY_SIZE = 0;

    @Autowired
    private FrequencyService frequencyService;

    @Value("${client-url}")
    private String clientUrl;

    public FrequencyController() {
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFrequencyById(@PathVariable String id) {
        Optional<byte[]> optionalFrequency = frequencyService.getFrequencyById(id);

        if (!optionalFrequency.isPresent()) {
            return getResponseEntityBodyBuilder(HttpStatus.NOT_FOUND, MediaType.APPLICATION_OCTET_STREAM)
                    .body(new byte[EMPTY_ARRAY_SIZE]);
        }

        return getResponseEntityBodyBuilder(HttpStatus.OK, MediaType.APPLICATION_OCTET_STREAM)
                .body(optionalFrequency.get());
    }

    @GetMapping("/getAll")
    public ResponseEntity<String> getAllIds() {
        JSONArray ja = new JSONArray(frequencyService.getAllIds());

        return getResponseEntityBodyBuilder(HttpStatus.OK, MediaType.APPLICATION_JSON)
                .body(ja.toString());
    }

    @PostMapping("/save")
    public ResponseEntity<String> generateAndSaveFrequencyByName(@RequestParam String id, @RequestParam int size) {
        HttpStatus httpStatus = frequencyService.generateAndSaveFrequency(id, size) ? HttpStatus.OK : HttpStatus.BAD_REQUEST;

        return getResponseEntityBodyBuilder(httpStatus, MediaType.TEXT_PLAIN).build();
    }

    @GetMapping("/isExists")
    public ResponseEntity<String> isExists(@RequestParam String id) {
        return getResponseEntityBodyBuilder(HttpStatus.OK, MediaType.TEXT_PLAIN)
                .body(frequencyService.isExists(id).toString());
    }

    private ResponseEntity.BodyBuilder getResponseEntityBodyBuilder(HttpStatus status, MediaType contentType) {
        return ResponseEntity
                .status(status)
                .contentType(contentType)
                .header("Access-Control-Allow-Origin", clientUrl);
    }
}