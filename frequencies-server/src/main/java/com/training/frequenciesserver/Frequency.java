package com.training.frequenciesserver;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(name = "frequencies")
public class Frequency {

    @Id
    private String id;

    private byte[] frequency;
}