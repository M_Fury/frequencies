package com.training.frequenciesserver;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface FrequencyRepository extends CrudRepository<Frequency, String> {
    @Query(value = "SELECT id FROM frequencies", nativeQuery = true)
    String[] findAllIds();
}