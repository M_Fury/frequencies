package com.training.frequenciesserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FrequencyService {
    private static final Integer[] VALID_FREQUENCIES_SIZES = {32, 64};

    @Autowired
    private FrequencyRepository frequenciesRepository;

    public Optional<byte[]> getFrequencyById(String id) {
        Optional<Frequency> frequency = frequenciesRepository.findById(id);

        return frequency.map(Frequency::getFrequency);
    }

    public String[] getAllIds() {
        return frequenciesRepository.findAllIds();
    }

    public boolean generateAndSaveFrequency(String id, int size) {
        if (!validSize(size) || isExists(id) || id.isEmpty()) {
            return false;
        }

        byte[] frequency = new byte[size];
        new Random().nextBytes(frequency);

        frequenciesRepository.save(new Frequency(id, frequency));

        return true;
    }

    public Boolean isExists(String id) {
        return frequenciesRepository.findById(id).isPresent();
    }

    private boolean validSize(Integer frequencySize) {
        return Arrays.asList(VALID_FREQUENCIES_SIZES).contains(frequencySize);
    }
}